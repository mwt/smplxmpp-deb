set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

set(SMPLXMPP_DEFAULT_EXTRA_CACERTS "" CACHE STRING "default location of additional CA certs in PEM format (a file)")

add_library(smplxmpp_core STATIC
    src/errorhandling.cc
    include/errorhandling.h
    src/chat.cc
    include/chat.h
    src/muc.cc
    include/muc.h
    src/xmpp.cc
    include/xmpp.h
    src/cfg_file.cc
    include/cfg_file.h
    src/cfg_cmdline.cc
    include/cfg_cmdline.h
    )
target_compile_features(smplxmpp_core PUBLIC cxx_std_11)
target_compile_options(smplxmpp_core PRIVATE -Werror -Wall -Wextra)

target_link_libraries(smplxmpp_core PRIVATE "${SMPLXMPP_SPDLOG_LIBS}")
target_link_libraries(smplxmpp_core PUBLIC
    smplxmpp_util
    gloox
    )

target_include_directories(smplxmpp_core PUBLIC
    include/
    "${PROJECT_BINARY_DIR}/include/"
    )
