set(SMPLXMPP_LICENSE_INSTALL_DIR "${CMAKE_INSTALL_FULL_DOCDIR}" CACHE STRING "directory the license file (used for reference in man page)")
set(SMPLXMPP_LICENSE_INSTALL_FILENAME "LICENSE" CACHE STRING "name the license file (used for reference in man page)")

option(SMPLXMPP_INSTALL_LICENSE "iff ON, an own copy of the license file will be installed" ON)

if (SMPLXMPP_INSTALL_LICENSE)
    install(FILES
        "COPYING" RENAME "${SMPLXMPP_LICENSE_INSTALL_FILENAME}"
        DESTINATION "${SMPLXMPP_LICENSE_INSTALL_DIR}"
        )

endif()

set(SMPLXMPP_LICENSE_INSTALL_LOCATION "${SMPLXMPP_LICENSE_INSTALL_DIR}/${SMPLXMPP_LICENSE_INSTALL_FILENAME}")
