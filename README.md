# smplxmpp
simple XMPP command line client

`smplxmpp` allows you to send and receive xmpp messages via the commandline.
Both normal chat messages and MUCs are supported.

This is another tool in your linux toolbelt:
To have it's output and/or input joined to other line-oriented tools like `sed` or `awk` in a pipeline.

## Examples
Please have a look at [the Tutorial](doc/tutorial.md) for more detailed introduction.

### Fully configured
This example shows sending and receiving messages when using a config file.

```
# send a message to alice
$ echo "alice@example.com Hello, World!" | smplxmpp

# now wait for a response interactively (and clear the offline queue)
$ smplxmpp
alice@example.com Hello Bot!
alice@example.com How are you doing?
```

### Out of the box
This example will work out of the box.

Note that you should not do that, it will leak your password to all users of the system (via the process list).

```
$ smplxmpp -j alice@example.com --pass=hackme
```

### MUC
Don't forget to specify a nick.
Also, parsing nicks in MUCs is almost impossible (they basically can contain any character), so they are **not displayed** (so you don't have to write complex parsers for `smplxmpp`'s output)

```
$ smplxmpp --muc "muc@conference.example.com/nick"
```

### Only one user
To only send and receive messages from a single user use `--focus`

```
$ smplxmpp --focus "bob@example.com"
Hello!
There are no more prefixes!
```


### Encoding helper
Every line contains only one message.
Newlines within a message are encoded to `\n`.

To encode a message you may pipe it through `smplxmpp-nl`:

```
$ fortune | smplxmpp-nl | smplxmpp --focus "bob@example.com"
```

Use the flag `-d` to decode messages.

### Configuration file
Place these in `/etc/smplxmpp.conf` (system wide) or in `~/.smplxmpprc` (only user).

```
jid alice@example.com
pass hackme
```

## Documentation
For the usage please refer to the man pages `smplxmpp(1)` for the binary, `smplxmpp(5)` for the configuration and `smplxmpp-nl(1)` for the encoding and decoding helper.

```
man 1 smplxmpp
man 5 smplxmpp
man 1 smplxmpp-nl
```

Additional documentation on the internal structure is provided in the `doc/` directory.

Activate the build option `BUILD_DOCUMENTATION` to build the API documentation.

## Install
This is a cmake project, and can be installed like any other cmake project.

```
git clone https://codeberg.org/tropf/smplxmpp.git
cd smplxmpp
mkdir build && cd build
cmake ..
ccmake . # optional, adjust build vars
make -j8
sudo make install
```

See [the docs](doc/buildsystem.md) for available build configuration.

### Dependencies
You require `cmake` and a c++ compiler (e.g. `g++`).

As libraries you need [gloox](https://camaya.net/gloox/) and [spdlog](https://github.com/gabime/spdlog/).
If you don't have them installed you can set `SMPLXMPP_GLOOX_USE_SYSTEM` and `SMPLXMPP_SPDLOG_USE_SYSTEM` to `OFF` respectively, which causes smplxmpp to fetch them via the internet and build them locally.

As transitive dependencies [libidn](https://www.gnu.org/software/libidn/), [zlib](https://zlib.net/) and [gnutls](https://gnutls.org/) are also required.
(Don't forget their header files for building.)

## License
This project is available under the GNU GPLv3 only.

## Online
Find this project on [Codeberg](https://codeberg.org/tropf/smplxmpp).

