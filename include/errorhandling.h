#ifndef __SMPLXMPP_ERRORHANDLING_H_INCLUDED__
#define __SMPLXMPP_ERRORHANDLING_H_INCLUDED__

#include <string>
#include <gloox/gloox.h>

using namespace gloox;
using namespace std;

/**
 * turns a gloox connection error into a printable string
 * @param e gloox error object
 * @return human readable string
 */
string glooxErrorToText(ConnectionError e);

/**
 * turns a gloox stream error into a printable string
 * @param e gloox error object
 * @return human readable string
 */
string glooxErrorToText(StreamError e);

/**
 * turns a gloox stanza error type into a printable string
 * @param e gloox error object
 * @return human readable string
 */
string glooxErrorToText(StanzaErrorType e);

/**
 * turns a gloox stanza error into a printable string
 * @param e gloox error object
 * @return human readable string
 */
string glooxErrorToText(StanzaError e);

/**
 * turns a gloox authentication error into a printable string
 * @param e gloox error object
 * @return human readable string
 */
string glooxErrorToText(AuthenticationError e);

#endif // __SMPLXMPP_ERRORHANDLING_H_INCLUDED__
