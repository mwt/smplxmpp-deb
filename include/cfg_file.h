#ifndef __SMPLXMPP_CFG_FILE_H_INCLUDED__
#define __SMPLXMPP_CFG_FILE_H_INCLUDED__

#include <argp.h>
#include <string>
#include <set>

using namespace std;

/**
 * loads single cmdline option from file
 * @param options argp option array
 * @param line line to parse
 * @param lineinfo will be printed to identify the line if an error occurs
 * @param forbidden_keys keys that will be not allowed when parsing the line
 */
void load_opt_from_line(const struct argp_option* options,
                        const string& line,
                        const string& lineinfo="\b",
                        const set<string>& forbidden_keys = {"verbosity", "quiet"});

/**
 * load a single cfg file
 * @param options argp option array
 * @param filename path to file to load
 * @param fail_loudly exit the program if the file can't be loaded, otherwise log to debug
 */
void load_cfg_file(const struct argp_option* options, string filename, bool fail_loudly = true);

/**
 * will load the cfg files (global, local) on first call.
 * will do nothing after that.
 */
void load_cfg_files_once();

#endif // __SMPLXMPP_CFG_FILE_H_INCLUDED__
