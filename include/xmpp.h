#ifndef __SMPLXMPP_XMPP_H_INCLUDED__
#define __SMPLXMPP_XMPP_H_INCLUDED__

#include <atomic>
#include <vector>
#include <mutex>
#include <chrono>

#include <gloox/client.h>
#include <gloox/connectionlistener.h>
#include <gloox/gloox.h>
#include <gloox/iqhandler.h>
#include <gloox/loghandler.h>

using namespace std;
using namespace gloox;

/**
 * struct to hold all necessary configuration information required during the execution
 */
struct SmplXmppHandlerConfig {
    /**
     * server name to connect to (resolved by DNS)
     */
    string server = "";

    /**
     * port to be used for connecting.
     * if left at default will be automatically determined.
     */
    unsigned short int port = 0;

    /**
     * behaviour regarding TLS.
     * should be left at default, only change for testing
     */
    TLSPolicy tlsPolicy = TLSPolicy::TLSRequired;

    /**
     * iff true any (even invalid) tls certs will be accepted
     * should be left at default, only change for testing
     */
    bool tlsIgnoreCertCheck = false;

    /**
     * specify paths to CA certs (in PEM format) to use
     */
    StringList caCerts;

    /**
     * if false: any received messages will not be printed to stdout
     *
     * note: this is achieved by not connecting the message handler
     */
    bool enableOutput = true;

    /**
     * if false: any input on stdin will be ignored
     */
    bool enableInput = true;

    /**
     * true if a muc is being used
     * set while handling cmdline args (w/ argp)
     */
    bool mucMode = false;

    /**
     * full JID of a muc to connect to.
     * should contain a nickname (resourcepart)
     */
    string mucJidString;

    /**
     * timeout for connection snippets in us.
     * after that time all not yet flushed messages will be sent to the server
     */
    unsigned timeoutFlush = 100000;

    /**
     * timeout for connection teardown in us.
     *
     * note: this timeout will not begin to count before the current flush cycle (see timeoutFlush) is completed
     */
    unsigned timeoutTeardown = 2500000;

    /**
     * time between heartbeats in seconds
     */
    unsigned heartbeatInterval = 59;

    /**
     * time to wait in us during stream setup before connection flushing attempt
     */
    unsigned timeoutSetupFlush = 5000000;

    /**
     * list of users that we are currently focusing on.
     * resourcepart will be ignored
     * empty: don't focus on any users
     */
    vector<JID> focusedUsers;
};
typedef struct SmplXmppHandlerConfig SmplXmppHandlerConfig;

/**
 * to be used as a default param for when the handler is instanced
 */
const SmplXmppHandlerConfig smplXmppHandlerConfigDefault;

/**
 * minimal information required for a message to be sent.
 * used in the message queue
 */
struct MsgPrototype {
    /**
     * recipient jid.
     * ignored for mucs
     */
    string jid;

    /**
     * actual message content.
     * may contain special characters such as \n (newline)
     */
    string body;
};
typedef struct MsgPrototype MsgPrototype;

/**
 * Stores the State of the Teardown Handshake
 */
enum TeardownHandshakeState {
    /**
    * Handshake has not been started yet.
    */
    NotStarted,

    /**
    * Initial message has been sent, but no response has been received.
    */
    IQSent,

    /**
    * Handshake has been completed.
    */
    Done,
};

/**
 * base class that manages the connection setup/teardown
 */
class SmplXmppHandlerBase : public ConnectionListener, LogHandler, IqHandler {
    protected:
        /**
         * Object to hold connection information
         */
        Client j;

        /**
         * queue of messages to be sent
         * may only be cleared by the main thread due to deadlocking concerns
         */
        list<MsgPrototype> msgQueue;

        /**
         * Mutex to ensure msgQueue is only ever accessed by one thread at a time.
         */
        mutex msgQueueMutex;

        /**
         * will be set to true as soon as message stanzas can be sent
         */
        bool streamReady = false;

        /**
         * used to communicate to the main thread: stop working now
         */
        atomic<bool> keepRunning;

        /**
         * saves the configuration passed via command line arguments
         */
        SmplXmppHandlerConfig cfg;

        /**
         * sends out a single message.
         * generally called from flushMessageQueue().
         * Implemented by children according to their functionality.
         * @param msg the message to send
         */
        virtual void sendSingleMsg(const MsgPrototype& msg);

        /**
        * State of the teardown handshake.
        */
        atomic<TeardownHandshakeState> teardownHandshakeState;

        /**
        * ID of the IQ used for the teardown handshake.
        */
        string teardownHandshakeIQID = "";

        /**
        * stores point in time of last heartbeat
        */
        chrono::time_point<chrono::steady_clock> lastHeartbeat;

        /**
        * stores last time of activity during setup (e.g. establishing TLS)
        */
        chrono::time_point<chrono::steady_clock> lastSetupActivity;

    public:
        /**
         * Constructor.
         * Connects all necessary handlers and does other init.
         * Does not open a server connection.
         * @param jid JID to use for auth
         * @param pass password to use for auth
         * @param given_cfg config to be used
         */
        SmplXmppHandlerBase(JID jid, string& pass, const SmplXmppHandlerConfig& given_cfg = smplXmppHandlerConfigDefault);

        /**
         * destructor
         */
        ~SmplXmppHandlerBase() {};

        /**
         * starts the connection to the server in the current thread.
         * blocking.
         */
        virtual void start();

        /**
         * tells the thread running the connection to stop
         */
        virtual void stop();

        /**
         * called on connection establishment.
         * writes to log.
         */
        virtual void onConnect();

        /**
         * called on disconnect.
         * contains only error handling.
         * @param e disconnect reason. not necessarily an error.
         */
        virtual void onDisconnect(ConnectionError e);

        /**
         * called on TLS init.
         * contains only error handling (and debug logging if enabled).
         * @param info information about the used certificate
         */
        virtual bool onTLSConnect(const CertInfo& info);

        /**
         * does plenty of checks to see if the stream is ready to send/recv messages
         * @return if we can write messages
         */
        virtual bool isStreamReady();

        /**
         * cleans up the message queue: removes messages with empty body.
         */
        virtual void tidyMessageQueue();

        /**
         * sends all messages from the message queue via the gloox interface.
         * they will not be sent out until the next cycle inside the start() method
         */
        virtual void flushMessageQueue();

        /**
         * used to detect when stream setup is complete
         * @param event thing that happened
         */
        virtual void onStreamEvent(StreamEvent event);

        /**
         * passes gloox log events to smplxmpp logger
         * @param level loglevel
         * @param area area
         * @param message message
         */
        virtual void handleLog(LogLevel level, LogArea area, const std::string& message);

        /**
         * enqueues a chat message.
         * constructs a MsgPrototype object and calls itself again.
         * @param jid recipient
         * @param body content to send
         */
        virtual void enqueueMsg(const string& jid, const string& body);

        /**
         * enqueues a chat message.
         * @param msg struct containing information to send msg
         */
        virtual void enqueueMsg(const MsgPrototype& msg);

        /**
         * Handles incoming IQs.
         * Ignores them usually, just required for the teardown handshake.
         * @param iq received IQ
         */
        virtual bool handleIq(const IQ& iq);

        /**
         * Implemented to satisfy gloox API.
         * Does nothing.
         * @param iq incoming iq
         * @param context iq context
         */
        virtual void handleIqID(const IQ& iq, int context);

        /**
         * Initializes the teardown handshake.
         * Sends a faulty IQ to the server and wait for corresponding "Error" IQ.
         */
        virtual void teardownHandshakeInit();
    
        /**
         * Returns True iff teardown handshake has been initialized and is done.
         * @return whether teardown handshake is done
         */
        virtual bool teardownHandshakeDone();

        /**
         * Returns True iff teardown handshake has been started in the past.
         * Will return true even after the handshake is completed.
         * @return whether teardown handshake has started
         */
        virtual bool teardownHandshakeStarted();

        /**
         * send a heartbeat if time since last heartbeat is greater than configured interval.
         * Does nothing if else (or if heartbeats are disabled).
         */
        virtual void sendHeartbeatMaybe();

        /**
         * if enough time since the last connection activity during setup has passed:
         * will attempt to flush the connection.
         * Else does nothing.
         */
        virtual void setupFlushMaybe();
};

#endif // __SMPLXMPP_XMPP_H_INCLUDED__
