# Setup Flush
During the setup of the stream some data might become "stuck" in some queue inside gloox or the network stack, especially in older versions of gloox.

To flush that queue additional data has to be enqueued to "push out" the stuck data.

This is achieved by waiting a configurable amount of time (default: 5 seconds) after the last stream event (init crypto etc.),
and if the stream is not established after this time sending a errornous IQ, which according to RFC 6120 must be responded to with an error.
This error response is the new data that pushes the old data out of the queue.

This will fire also on very slow authentication procedures, but do no harm.

It is hacky, but required to support versions of gloox that are still in use.
