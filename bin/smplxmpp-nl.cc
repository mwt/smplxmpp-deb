#include <iostream>
#include <string>
#include <argp.h>

#include <config.h>
#include <util.h>

using namespace std;

const char* argp_program_version = SMPLXMPP_VERSION;
const char* argp_program_bug_address = SMPLXMPP_BUG_ADDRESS;

enum operationMode {
                    encode,
                    decode,
};

operationMode opmode = operationMode::encode;
bool printFinalNl = true;

static int parse_opt(int key, char* arg, struct argp_state* state) {
    switch(key) {
    case 'd':
        opmode = operationMode::decode;
        break;
    case 'n':
        printFinalNl = false;
        break;
    }

    return 0;
}

int main(int argc, char** argv) {
    struct argp_option options[] =
        {
         {"decode", 'd', 0, 0, "decode given text"},
         {"no-newline", 'n', 0, 0, "do not print newline at the end"},
         {0}
        };
    struct argp argp = {options, parse_opt, "", "en/decode messages for smplxmpp\vEncoding (default behaviour)\nreplaces backslashes (\\) with \\\\ and newlines with \\n\n\nDecoding\nreverse the encoding\n\nWorks solely on standard input, does not accept file names.\nAfter the en/decoding is done, a newline will be appended by default.\n\nBuilt by Rolf Pfeffertal."};

    int argp_result = argp_parse(&argp, argc, argv, 0, 0, 0);
    if (argp_result != 0) {
        return argp_result;
    }

    for (string line; getline(cin, line);) {
        line += "\n";

        if (operationMode::encode == opmode) {
            cout << newlinesEncode(line);
        } else {
            cout << newlinesDecode(line);
        }
    }

    if (printFinalNl) {
        cout << endl;
    }

    return 0;
}
