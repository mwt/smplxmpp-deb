#include <muc.h>

#include <iostream>
#include <list>

#include <spdlog/spdlog.h>

#include <gloox/client.h>
#include <gloox/error.h>
#include <gloox/gloox.h>
#include <gloox/message.h>
#include <gloox/mucroom.h>

#include <util.h>
#include <errorhandling.h>

SmplXmppHandlerMuc::SmplXmppHandlerMuc(JID jid, string& pass, const SmplXmppHandlerConfig& given_cfg):
    SmplXmppHandlerBase(jid, pass, given_cfg),
    m_room(&j, JID(cfg.mucJidString), this, 0) {
    if (!cfg.mucMode) {
        spdlog::critical("instanciated MUC handler, but not in muc room");
        exit(1);
    }
}

void SmplXmppHandlerMuc::onConnect() {
    m_room.join();

    flushMessageQueue();
}

bool SmplXmppHandlerMuc::isStreamReady() {
    if (!SmplXmppHandlerBase::isStreamReady()) {
        return false;
    }

    if (m_room.role() == MUCRoomRole::RoleNone) {
        return false;
    }

    return true;
}

void SmplXmppHandlerMuc::handleMUCMessage( MUCRoom* /*room*/, const Message& msg, bool priv ) {
    spdlog::trace("incoming muc msg from '{}', (history: {}, private: {}): '{}'", msg.from().resource(), msg.when() ? 'y':'n', priv ? 'y':'n', msg.body());

    if (teardownHandshakeStarted()) {
        spdlog::debug("incoming muc msg after shutdown has been initiated. ignoring.");
        return;
    }

    // only print message if it is neither private, nor in history
    if (msg.when() || priv) {
        return;
    }

    // check if i have sent that message
    if (!msgsSentByMe.empty()
        && msg.from().resource() == m_room.nick()
        && msg.body() == msgsSentByMe.front()) {
        spdlog::trace("discarding message, receiving duplicate of own sent msg");
        msgsSentByMe.pop_front();
        return;
    }

    if (cfg.enableOutput) {
        cout << newlinesEncode(msg.body()) << endl;
    }
}

void SmplXmppHandlerMuc::handleMUCError( MUCRoom * /*room*/, StanzaError error ) {
    spdlog::error("got muc error: {}", glooxErrorToText(error));
}

bool SmplXmppHandlerMuc::handleMUCRoomCreation( MUCRoom *room ) {
    spdlog::info("room '{}' didn't exist, created.", room->name());
    return true;
}

void SmplXmppHandlerMuc::handleMUCParticipantPresence( MUCRoom*, const MUCRoomParticipant, const Presence&) {
    // this bot does not track coming and going of people.
    // it knows what data minimization is.
}

void SmplXmppHandlerMuc::handleMUCSubject( MUCRoom*, const std::string&, const std::string& subject) {
    spdlog::info("room subject is: '{}'", subject);
}

void SmplXmppHandlerMuc::handleMUCInfo( MUCRoom*, int features, const std::string& name, const DataForm* infoForm) {
    spdlog::debug("muc info, features: {}, name: '{}', form xml: '{}'", features, name, infoForm->tag()->xml());
}

void SmplXmppHandlerMuc::handleMUCItems( MUCRoom*, const Disco::ItemList&) {
    // this bot does not track coming and going of people.
    // it knows what data minimization is.
}

void SmplXmppHandlerMuc::handleMUCInviteDecline( MUCRoom*, const JID&, const std::string&) {
    // this bot does not invite people.
    // only implemented so this class isn't abstract.
}

void SmplXmppHandlerMuc::sendSingleMsg(const MsgPrototype& msg) {
    spdlog::trace("sending message to muc: '%s'", msg.body);
    msgsSentByMe.push_back(msg.body);
    m_room.send(msg.body);
}
