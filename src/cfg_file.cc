#include <cfg_file.h>

#include <string>
#include <fstream>
#include <regex>

#include <spdlog/spdlog.h>

#include <cfg_cmdline.h>
#include <util.h>

using namespace std;

void load_opt_from_line(const struct argp_option* options,
                        const string& line,
                        const string& lineinfo,
                       const set<string>& forbidden_keys) {
    spdlog::trace("parsing cfg file entry {}: {}", lineinfo, line);

    regex regex_empty_line("^\\s*$");
    regex regex_comment("^\\s*#.*$");
    smatch m;

    if (regex_match(line, m, regex_comment)) {
        spdlog::trace("...is comment");
        return;
    }

    if (regex_match(line, m, regex_empty_line)) {
        spdlog::trace("...is empty");
        return;
    }

    regex regex_regular("^\\s*(\\S+)\\s*(\\S.*)?$");

    if (!regex_match(line, m, regex_regular)) {
        spdlog::error("cfg {}: entry does not conform to format '{}'", lineinfo, line);
        return;
    }

    string key = m[1];
    string val = m[2];

    if (forbidden_keys.find(key) != forbidden_keys.end()) {
        spdlog::error("cfg {}: using the key '{}' is not allowed in cfg files", lineinfo, key);
        return;
    }

    struct argp_state s;
    
    int argp_key = 0;
    for (int i = 0; !is_empty_argp_option(options[i]); i++) {
        if (options[i].name != NULL && key == options[i].name) {
            if (0 != (options[i].flags & OPTION_ALIAS)) {
                // is alias: use previous key
                parse_opt(argp_key, (char *) val.c_str(), &s);
            } else {
                // is not alias: use current key
                parse_opt(options[i].key, (char *) val.c_str(), &s);
            }

            return;
        }
        if (options[i].key != 0) {
            argp_key = options[i].key;
        }
    }

    spdlog::error("cfg {}: key '{}' is unknown", lineinfo, key);
}

void load_cfg_file(const struct argp_option* options, string filename, bool fail_loudly) {
    ifstream f(filename);

    if (f.is_open()) {
        spdlog::info("loading cfg file {}", filename);
        string line;
        int line_num = 1;
        while (getline(f, line)) {
            load_opt_from_line(options, line, filename + ":" + to_string(line_num));
            line_num++;
        }
    } else {
        if (fail_loudly) {
            spdlog::critical("cfg file {} not found/readable", filename);
            exit(1);
        } else {
            spdlog::debug("cfg file {} not found/readable (carrying on)", filename);
        }
    }
}

void load_cfg_files_once() {
    static bool loaded = false;

    if (!loaded) {
        loaded = true;

        spdlog::debug("loading default cfg files (global: {}, local: {})", SMPLXMPP_CFG_GLOBAL, SMPLXMPP_CFG_LOCAL);
        load_cfg_file(options, SMPLXMPP_CFG_GLOBAL, false);
        load_cfg_file(options, SMPLXMPP_CFG_LOCAL, false);
    }
}
