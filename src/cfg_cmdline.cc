#include <cfg_cmdline.h>

#include <argp.h>
#include <string>
#include <fstream>
#include <memory>
#include <cstdio>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include <gloox/gloox.h>

#include <config.h>
#include <cfg_file.h>
#include <xmpp.h>
#include <util.h>

using namespace std;
using namespace gloox;

const char* argp_program_bug_address = SMPLXMPP_BUG_ADDRESS;
void (*argp_program_version_hook)(FILE*, struct argp_state*) = &smplxmppPrintVersion;

// just exists. does not get any init.
struct SmplXmppGlobalOptions opts;

const int ARGP_PASSWORD = 1337;
const int ARGP_TLS_REQUIRED = 1338;
const int ARGP_TLS_OPTIONAL = 1339;
const int ARGP_TLS_DISABLED = 1340;
const int ARGP_NO_INPUT = 1341;
const int ARGP_NO_OUTPUT = 1342;
const int ARGP_LOGFILE_INFO = 1343;
const int ARGP_LOGFILE_WARNING = 1344;
const int ARGP_LOGFILE_DEBUG = 1345;
const int ARGP_TIMEOUT_FLUSH = 1346;
const int ARGP_TIMEOUT_TEARDOWN = 1347;
const int ARGP_HEARTBEAT_INTERVAL = 1349;
const int ARGP_SETUP_FLUSH = 1350;
const int ARGP_TLS_IGNORE_CERT_CHECK = 1351;
const int ARGP_TLS_CACERT = 1352;

// so, argp uses "any non-specified field in a struct is 0" quite heavily
// but that causes an error with g++ (technically a warning)
// so this will be turned off here


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
const struct argp_option options[] =
    {
     {0, 0, 0, 0, "Authentication Options", 70},
     {"jid", 'j', "JID", 0, "jabber id to login"},
     {"password", ARGP_PASSWORD, "PASSWORD", 0, "password to login (strongly discouraged)"},
     {"pass", 0, 0, OPTION_ALIAS, 0},

     {0, 0, 0, 0, "Connection Options", 100},
     {"server", 's', "ADDRESS", 0, "address of the server to connect to"},
     {"port", 'p', "PORT", 0, "port of the server"},
     {"tls-required", ARGP_TLS_REQUIRED, 0, 0, "enforce TLS usage (default)"},
     {"tls-optional", ARGP_TLS_OPTIONAL, 0, 0, "allow (but not enforce) TLS"},
     {"tls-disabled", ARGP_TLS_DISABLED, 0, 0, "disable TLS (strongly discouraged)"},
     {"tls-ignore-cert-check", ARGP_TLS_IGNORE_CERT_CHECK, 0, 0, "skip TLS cert validation"},
     {"tls-cacert", ARGP_TLS_CACERT, "FILE", 0, "use cert in PEM format as cert authority, can be used multiple times"},
     {"heartbeat", ARGP_HEARTBEAT_INTERVAL, "SECONDS", 0, "time between heartbeats (application-level keepalive), 0 to disable"},
     {"timeout-setup-flush", ARGP_SETUP_FLUSH, "MICROSECONDS", 0, "time to wait before attempting to flush the connection during setup, 0 to disable"},

     {0, 0, 0, 0, "I/O Options", 130},
     {"no-input", ARGP_NO_INPUT, 0, 0, "ignore all input"},
     {"no-output", ARGP_NO_OUTPUT, 0, 0, "don't display received messages. note that messages will still be received"},
     {"muc", 'm', "JID", 0, "use the given muc. the program can either use a muc or message specific users. must be in the form muc@domain.tld/nick"},
     {"focus", 'f', "JID", 0, "only interact with given user(s). format works like in mucs (no leading JID on input/output). ignores all other messages"},
     {"timeout-flush", ARGP_TIMEOUT_FLUSH, "MICROSECONDS", 0, "maximum time between flushing messages from stdin to server"},
     {"timeout-teardown", ARGP_TIMEOUT_FLUSH, "MICROSECONDS", 0, "maximum time to wait for network after shutdown started"},

     {0, 0, 0, 0, "Logging Options", 150},
     {"verbosity", 'v', "LEVEL", 0, "set minimal log level for stderr (ERROR, WARNING, INFO, DEBUG, TRACE)"},
     {"quiet", 'q', 0, 0, "show no log messages (does not imply --no-output)"},
     {"logfile", ARGP_LOGFILE_WARNING, "FILE", 0, "specify logfile for warnings and above"},
     {"logfile-info", ARGP_LOGFILE_INFO, "FILE", 0, "specify logfile for infos and above"},
     {"logfile-debug", ARGP_LOGFILE_DEBUG, "FILE", 0, "specify logfile for debug messages"},

     {0, 0, 0, 0, "Other Options", -1},
     {"cfg-file", 'c', "FILE", 0, "load additional config file"},
     {0}
    };
#pragma GCC diagnostic pop

int parse_opt(int key, char* arg, struct argp_state*) {
    ifstream passfile;
    ifstream cfgfile;
    string arg_s = "";
    shared_ptr<spdlog::sinks::basic_file_sink_mt> file_sink_info;
    shared_ptr<spdlog::sinks::basic_file_sink_mt> file_sink_warn;
    shared_ptr<spdlog::sinks::basic_file_sink_mt> file_sink_dbg;
    if (nullptr != arg) {
        arg_s = arg;
    }

    // before trying to read any options try to load the cfg files
    // except in some cases (control events, setting verbosity)
    // this allows setting verbosity before loading of cfg files
    switch(key) {
    case ARGP_KEY_END:
    case ARGP_KEY_ARGS:
    case ARGP_KEY_NO_ARGS:
    case ARGP_KEY_INIT:
    case ARGP_KEY_SUCCESS:
    case ARGP_KEY_ERROR:
    case ARGP_KEY_FINI:
    case 'v':
    case 'q':
        break;
    default:
        load_cfg_files_once();
    }

    switch(key) {
    case 'j':
        opts.jid = arg;
        break;
    case ARGP_PASSWORD:
        opts.pass = arg;
        break;
    case 's':
        opts.cfg.server = arg;
        break;
    case 'p':
        opts.cfg.port = atoi(arg);
        break;
    case ARGP_TLS_REQUIRED:
        opts.cfg.tlsPolicy = TLSPolicy::TLSRequired;
        break;
    case ARGP_TLS_OPTIONAL:
        opts.cfg.tlsPolicy = TLSPolicy::TLSOptional;
        break;
    case ARGP_TLS_DISABLED:
        opts.cfg.tlsPolicy = TLSPolicy::TLSDisabled;
        break;
    case ARGP_TLS_IGNORE_CERT_CHECK:
        opts.cfg.tlsIgnoreCertCheck = true;
        break;
    case ARGP_TLS_CACERT:
        opts.cfg.caCerts.push_back(arg_s);
        break;
    case ARGP_NO_OUTPUT:
        opts.cfg.enableOutput = false;
        break;
    case ARGP_NO_INPUT:
        opts.cfg.enableInput = false;
        break;
    case 'q':
        setStderrLoglevel(spdlog::level::off);
        break;

    case 'v':
        if ("INFO" == arg_s) {
            setStderrLoglevel(spdlog::level::info);
        } else if ("ERROR" == arg_s) {
            setStderrLoglevel(spdlog::level::err);
        } else if ("WARNING" == arg_s) {
            setStderrLoglevel(spdlog::level::warn);
        } else if ("DEBUG" == arg_s) {
            setStderrLoglevel(spdlog::level::debug);
        } else if ("TRACE" == arg_s) {
            setStderrLoglevel(spdlog::level::trace);
        } else {
            spdlog::error("unknown log level '{}'", arg_s);
        }
        break;

    case ARGP_LOGFILE_WARNING:
        file_sink_warn = make_shared<spdlog::sinks::basic_file_sink_mt>(arg, false);
        file_sink_warn->set_level(spdlog::level::warn);
        spdlog::default_logger()->sinks().push_back(file_sink_warn);
        break;
    case ARGP_LOGFILE_INFO:
        file_sink_info = make_shared<spdlog::sinks::basic_file_sink_mt>(arg, false);
        file_sink_info->set_level(spdlog::level::info);
        spdlog::default_logger()->sinks().push_back(file_sink_info);
        break;
    case ARGP_LOGFILE_DEBUG:
        file_sink_dbg = make_shared<spdlog::sinks::basic_file_sink_mt>(arg, false);
        file_sink_dbg->set_level(spdlog::level::trace);
        spdlog::default_logger()->sinks().push_back(file_sink_dbg);
        break;

    case 'c':
        // will exit if file does not exist
        load_cfg_file(options, arg, true);
        break;

    case 'm':
        if (opts.cfg.mucMode) {
            spdlog::critical("only a single muc can be joined at any time");
            exit(1);
        }

        opts.cfg.mucMode = true;
        opts.cfg.mucJidString = arg;
        break;

    case ARGP_TIMEOUT_FLUSH:
        opts.cfg.timeoutFlush = atoi(arg);
        break;
    case ARGP_TIMEOUT_TEARDOWN:
        opts.cfg.timeoutTeardown = atoi(arg);
        break;

    case ARGP_HEARTBEAT_INTERVAL:
        opts.cfg.heartbeatInterval = atoi(arg);
        break;
    case ARGP_SETUP_FLUSH:
        opts.cfg.timeoutSetupFlush = atoi(arg);
        break;

    case 'f':
        opts.cfg.focusedUsers.push_back(JID(arg));
        break;
    }

    return 0;
}

void smplxmppPrintVersion(FILE* f, struct argp_state*) {
    fprintf(f, "smplxmpp %s\ngloox %s\n", SMPLXMPP_VERSION, GLOOX_VERSION.c_str());
}
