#include <util.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <argp.h>

#include <chrono>
#include <iomanip>
#include <sstream>
#include <string>
#include <regex>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_sinks.h>

using namespace std;

string newlinesEncode(const string& s) {
    string backslashesDone = regex_replace(s, regex("\\\\"), "\\\\");
    return regex_replace(backslashesDone, regex("\\n"), "\\n");
}

string newlinesDecode(const string& s) {
    string newlinesDone = regex_replace(s, regex("\\\\n"), "\n");
    return regex_replace(newlinesDone, regex("\\\\\\\\"), "\\");
}

string getHomeDir() {
    const char *homedir;

    if ((homedir = getenv("HOME")) == NULL) {
        homedir = getpwuid(getuid())->pw_dir;
    }

    return string(homedir);
}

bool is_empty_argp_option(const struct argp_option& o) {
    return 0 == o.name &&
        0 == o.key &&
        0 == o.arg &&
        0 == o.flags &&
        0 == o.doc &&
        0 == o.group;
}

void initSpdlog() {
    spdlog::default_logger()->sinks().clear();

    auto stderr_sink = make_shared<spdlog::sinks::stderr_sink_mt>();
    // only print warnings to stdout
    stderr_sink->set_level(spdlog::level::warn);
    spdlog::default_logger()->sinks().push_back(stderr_sink);

    // ...but handle logs of *all* levels
    spdlog::default_logger()->set_level(spdlog::level::trace);
}

void setStderrLoglevel(spdlog::level::level_enum lvl) {
    spdlog::default_logger()->sinks().at(0)->set_level(lvl);
}

string getHumanTimePointFromEpoch(int t) {
    // okay, this is kinda a bad joke but here we go:
    // usually you'd just put t into a time_t and use ctime to format that but...
    // ctime is marked as obsolte and i am going to follow that
    // the c++-alternative to ctime is put_time (if we are not going to use bloody c-style-strings==char arrays),
    // which in turn requires us to use stringstreams
    // also just creating time_t by passing it a epoch time is *not* conforming to standard,
    // because the standard does not actually specify what the content of time_t is, you should not modify the content yourself
    // sometimes, fuck c++

    time_t t_struct = chrono::system_clock::to_time_t(chrono::time_point<chrono::system_clock>(chrono::seconds(t)));
    stringstream ss;
    ss << std::put_time(std::localtime(&t_struct), "%c %Z");

    return ss.str();
}
