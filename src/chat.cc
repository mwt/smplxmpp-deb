#include <chat.h>

#include <iostream>
#include <algorithm>

#include <spdlog/spdlog.h>

#include <gloox/client.h>
#include <gloox/carbons.h>
#include <gloox/disco.h>
#include <gloox/error.h>
#include <gloox/forward.h>
#include <gloox/gloox.h>
#include <gloox/message.h>
#include <gloox/messageeventfilter.h>
#include <gloox/receipt.h>
#include <gloox/stanza.h>
#include <gloox/stanzaextension.h>

#include <util.h>
#include <xmpp.h>

using namespace std;
using namespace gloox;

SmplXmppHandlerChat::SmplXmppHandlerChat(JID jid, string& pass, const SmplXmppHandlerConfig& given_cfg):
    SmplXmppHandlerBase(jid, pass, given_cfg),
    m_session(0),
    m_messageEventFilter(0) {
    j.disco()->registerDiscoHandler(this);

    j.registerStanzaExtension(new Forward());
    j.registerStanzaExtension(new Carbons());
    j.registerStanzaExtension(new Receipt(Receipt::ReceiptType::Invalid));

    if (cfg.enableOutput) {
        spdlog::trace("attached message handler");
        j.registerMessageSessionHandler(this, 0);
    } else {
        spdlog::trace("did not attach message handler");
    }
}

void SmplXmppHandlerChat::onConnect() {
    SmplXmppHandlerBase::onConnect();

    // note: done here instead of base handler because we need to specify the disco handler -> which the base class doesn't know
    spdlog::trace("requesting disco info");
    j.disco()->getDiscoInfo(j.jid().server(), "", this, 133714);
}

bool isInUserList(const JID& needle, const vector<JID> haystack) {
    for (const auto& user : haystack) {
        if (user.bare() == needle.bare()) {
            return true;
        }
    }

    return false;
}

void SmplXmppHandlerChat::handleMessage(const Message& msg, MessageSession* session) {
    spdlog::trace("incoming message (type: {}) from '{}' (subject: '{}', thread: '{}'): '{}'", msg.subtype(), msg.from().full(), msg.subject(), msg.thread(), msg.body());

    if (teardownHandshakeStarted()) {
        spdlog::debug("received msg after shutdown has been initiated. ignoring.");
        return;
    }

    // handling carbons
    if (msg.hasEmbeddedStanza()) {
        const Carbons* carbon = msg.findExtension<const Carbons>(ExtCarbons);
        if (carbon && carbon->embeddedStanza()) {
            spdlog::debug("is a carbon msg, recalling handler with embedded msg");
            Message* embeddedMessage = static_cast<Message*>(carbon->embeddedStanza());

            handleMessage(*embeddedMessage, session);
            return;
        }
    }

    if (msg.from().bare() == j.jid().bare()) {
        spdlog::trace("handling message sent by me, ignoring");
        return;
    }

    if (msg.body().size() == 0) {
        spdlog::trace("received msg w/o body, ignoring");
        return;
    }

    // check if focused on specific users
    if (!cfg.focusedUsers.empty()) {
        if (!isInUserList(msg.from(), cfg.focusedUsers)) {
            spdlog::debug("discarding message: '{}' not in focused users", msg.from().bare());
            return;
        }
    }

    if (cfg.focusedUsers.empty()) {
        // normal snytax
        cout << msg.from().bare() << " " << newlinesEncode(msg.body()) << endl;
    } else {
        // focused user: drop user part
        cout << newlinesEncode(msg.body()) << endl;
    }

    const Receipt* receiptRequest = msg.findExtension<const Receipt>(StanzaExtensionType::ExtReceipt);
    if (receiptRequest && receiptRequest->rcpt() == Receipt::ReceiptType::Request) {
        spdlog::trace("receipt requested, acknowledging");
        Message ackMsg(msg.subtype(), msg.from());
        // note: ackMsg now owns the receipt and will clean it up
        ackMsg.addExtension(new Receipt(Receipt::ReceiptType::Received, msg.id()));
        j.send(ackMsg);
    }
}

void SmplXmppHandlerChat::handleMessageEvent(const JID& from, MessageEventType event) {
    spdlog::trace("received message event: {} from: '{}'", event, from.full());
}

void SmplXmppHandlerChat::handleMessageSession(MessageSession* session) {
    spdlog::trace("got new session");
    // this example can handle only one session. so we get rid of the old session
    j.disposeMessageSession(m_session);
    m_session = session;
    m_session->registerMessageHandler(this);
    m_messageEventFilter = new MessageEventFilter(m_session);
    m_messageEventFilter->registerMessageEventHandler(this);
}

void SmplXmppHandlerChat::handleDiscoInfo(const JID& from, const Disco::Info& info, int context) {
    spdlog::trace("disco info received (JID: '{}', context: {}), features:", from.full(), context);
    for (const auto & feature : info.features()) {
        spdlog::trace("* {}", feature);
    }

    // try once to check for carbons support
    if (!carbonsCheckedForExistence) {
        if (info.hasFeature(XMLNS_MESSAGE_CARBONS)) {
            spdlog::debug("carbons supported; trying to enable");

            IQ iq(IQ::Set, JID(), j.getID());
            iq.addExtension(new Carbons(Carbons::Enable));
            j.send(iq, nullptr, 1);
        } else {
            spdlog::debug("carbons not supported");
        }

        carbonsCheckedForExistence = true;
    }
}

void SmplXmppHandlerChat::handleDiscoItems(const JID& from, const Disco::Items&, int context) {
    spdlog::trace("disco item received (JID: '{}', context: {})", from.full(), context);
}

void SmplXmppHandlerChat::handleDiscoError( const JID& from, const Error* error, int context) {
    string errorString = "<no error>";
    if (0 != error) {
        errorString = error->text();
    }
    spdlog::warn("disco error (JID: '{}', context: {}): '{}'", from.full(), context, errorString);
}

void SmplXmppHandlerChat::sendSingleMsg(const MsgPrototype& msg) {
        Message msgToSend(Message::MessageType::Chat, JID(msg.jid), msg.body);
        spdlog::trace("sending message to '{}' (type: '{}'): '{}'", msg.jid, msgToSend.subtype(), msg.body);
        j.send(msgToSend);
}
